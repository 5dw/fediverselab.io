
---
layout: "post"
title: "Themed servers"
---

This is a list of themed servers. Currently it includes [Mastodon](/en/mastodon) and [Friendica](/en/friendica) servers. This will change in future updates.
Servers are not restricted to their theme. They only help people with common interests find their community.

<ul class="article-list">

#### 💡 [Sciences](#sciences)
* [scicomm.xyz](https://scicomm.xyz) - for scientists and science enthusiasts
* [qoto.org](https://qoto.org) - for scholars in Science, Technology, Engineering, and Mathematics (STEM)
* [scholar.social](https://scholar.social) - for researchers, undergrads, journal editors, librarians, administrators

#### 🎨 [Humanities](#humanities)
* [mastodon.cc](https://mastodon.cc) - for art
* [writing.exchange](https://writing.exchange) - community for poets, writers, bloggers
* [bookwor.ms](https://bookwor.ms) - for book-lovers
* [imaginair.es](https://imaginair.es) - for writers, photographers, painters, cartoonists and people with imagination
* [artalley.social](https://artalley.social) - for artists and commissioners
* [baraag.net](https://baraag.net) - all types of art, uncensored 18+
* [vis.social](https://vis.social) - for anyone into data, visualization, creative coding, related arts and research
* [photographer.pro](https://photographer.pro) - for working professional photographers
* [photog.social](https://photog.social) - place for your photos
* [oulipo.social](https://oulipo.social) - a lipogrammatic server
* [literatur.social](https://literatur.social) - for German speaking authors and literary people
* [mograph.social](https://mograph.social) - for motion design community, VFX / 3D artists, animators and illustrators
* [socel.net](https://socel.net) - for animation professionals and freelancers

#### 🎓 [Education](#education)
* [tusk.schoollibraries.net](https://tusk.schoollibraries.net) - helping educators improve the School Libraries Resource Network
* [openbiblio.social](https://openbiblio.social) - German libraries and information facilities
* [ausglam.space](https://ausglam.space) - Australian galleries, libraries, archives, museums and records people

#### 🙏 [Religion](#religion)
* [rcsocial.net](https://rcsocial.net) - for Catholics and those interested in Catholicism
* [theres.life](https://theres.life) - for Christians

#### 🎵 [Music](#music)
* [metalhead.club](https://metalhead.club) - for metal friends
* [linernotes.club](https://linernotes.club) - for discussing music recordings
* [diymusic.space](https://linernotes.club) - for those who make music
* [koreadon.com](https://koreadon.com) - for K-POP music fans

#### 🔭 [Hobbies](#hobbies)
* [makestuff.club](https://makestuff.club) - for people that make stuff
* [scramble.city](https://scramble.city) - transformers, super robots, mecha and related subjects
* [mhz.social](https://mhz.social) - for radio amateurs, DXers and anyone interested to geek out about radio
* [afu.social](https://afu.social) - for radio amateurs in German-speaking countries
* [mastodon.radio](https://mastodon.radio) - for Amateur (Ham) Radio community
* [dice.camp](https://dice.camp) - for tabletop gamers
* [boardgames.social](https://boardgames.social) - for board gamers
* [mastodon.beerfactory.org](https://mastodon.beerfactory.org) - for beermakers
* [motogp.space](https://motogp.space) - for MotoGP and motorbikes fans
* [mastd.racing](https://mastd.racing) - dedicated to motosports
* [ocr.social](https://ocr.social) - for the global Obstacle Course Running (OCR) community
* [witches.live](https://witches.live) - for witches
* [mmorpg.social](https://mmorpg.social) - for those who love MMOs
* [kith.kitchen](https://kith.kitchen) - food and cooking
* [rollenspiel.social](https://rollenspiel.social) - (German) roleplay, Pen & Paper, tabletop, TCG, all gamers can settle here

#### 🎏 [Language specific](#languages)
* [polyglot.city](https://polyglot.city) - for polyglots, multilinguals, interested in languages and translating
* [esperanto.masto.host](https://esperanto.masto.host) - for those interested in Esperanto
* [tooot.im](https://tooot.im) - main instance language is Hebrew
* [toot.si](https://toot.si) - Slovenian (multilingual) instance
* [mastodontti.fi](https://mastodontti.fi) - Finnish (public toots should be in Finnish)
* [fikaverse.club](https://fikaverse.club) - Swedish and other Scandinavian languages
* [mastodon.sk](https://mastodon.sk) - for Slovak users
* [the.wired.sehol.se](https://the.wired.sehol.se) - for Hungarian speakers
* [mastodon.uno](https://mastodon.uno) - for Italian speakers

#### ⛺ [Regional](#regional)
* [muenchen.social](https://muenchen.social) - Munich, Germany
* [mastodon.bayern](https://mastodon.bayern) - Bavaria, Germany
* [toot.koeln](https://toot.koeln) - Koeln, Germany (generalistic)
* [ruhr.social](https://ruhr.social) - Ruhr area, Germany
* [social.saarland](https://social.saarland) - Saarland, Germany
* [dresden.network](https://dresden.network) - Dresden, Germany
* [machteburch.social](https://machteburch.social) - Magdeburg, Germany
* [bonn.social](https://bonn.social) - Bonn, Germany
* [oslo.town](https://oslo.town) - Oslo, Norway
* [snabelen.no](https://snabelen.no) - Norway
* [mastodont.cat](https://mastodont.cat) - Catalonia
* [mastodon.uy](https://mastodon.uy) - Uruguay
* [mastodon.scot](https://mastodon.scot) - Scotland, or who identify as Scottish
* [net.scot](https://net.scot) - Scotland (experimental social network with a Scottish twist)
* [glasgow.social](https://glasgow.social) - Glasgow, Scotland
* [toot.wales](https://toot.wales) - Wales and the Welsh
* [mastodon.me.uk](https://mastodon.me.uk) - UK tech community
* [rva.party](https://rva.party) - Richmond, USA
* [mastodon.mg](https://mastodon.mg) - Madagascar
* [balkan.fedive.rs](https://balkan.fedive.rs) - the Balkans (generalistic)
* [mastodon.fedi.quebec](https://mastodon.fedi.quebec) - Quebec, Canada
* [mastodon.ml](https://mastodon.ml) - Russia
* [mastodos.com](https://mastodos.com) - Kyoto, Japan
* [kopiti.am](https://kopiti.am) - Singapore
* [mexicosocial.mx](https://mexicosocial.mx) - Mexico
* [mastodon.opencloud.lu](https://mastodon.opencloud.lu) - Luxembourgish / European private and public organisations
* [bagra.club](https://bagra.club) - Macedonia
* [bologna.one](https://bologna.one) - Italy
* [tamilsangam.social](https://tamilsangam.social) - India
* [tamiltoot.online](https://tamiltoot.online) - India

#### 🎬 [Game / Book / Show theme](#entertainment)
** [tenforward.social](https://tenforward.social) - theme Star Trek
* [kirakiratter.com](https://kirakiratter.com) - for Aikatsu fans
* [donphan.social](https://donphan.social) - for Pokémon enthusiasts
* [cmdr.social](https://cmdr.social) - dedicated to Elite: Dangerous

#### 🐧 [For techies](#servers-for-techies)
* [infosec.exchange](https://infosec.exchange) - infosec and IT security discussions
* [mastodon.technology](https://mastodon.technology) - for people interested in technology
* [fosstodon.org](https://fosstodon.org) - dedicated to Free and Open Source Software
* [floss.social](https://floss.social) - for people who support or build Free Libre Open Source Software
* [miaou.drycat.fr](https://miaou.drycat.fr) - for geeks, FOSS and cat lovers, [CHATONS](https://chatons.org) member
* [linuxrocks.online](https://linuxrocks.online) - dedicated to Linux and technologies
* [oldbytes.space](https://oldbytes.space) - for old hardware fans
* [mathstodon.xyz](https://mathstodon.xyz) - for Maths people
* [digipres.club](https://digipres.club) - conversations about digital preservation
* [ubuntu.social](https://ubuntu.social) - Ubuntu community
* [indieweb.social](https://indieweb.social) - for participants and supporters of the IndieWeb movement

#### 💻 [Programming](#instances-for-programmers)
* [ruby.social](https://ruby.social) - for people interested in Ruby, Rails and related topics
* [phpc.social](https://phpc.social) - PHP community
* [functional.cafe](https://functional.cafe) - for people interested in functional programming and languages
* [social.csswg.org](https://social.csswg.org) - for anyone interested in CSS or general web development

#### 🎮 [Gamedev](#gamedev)
* [mastodon.gamedev.place](https://mastodon.gamedev.place) - gamedev and related professions
* [gamemaking.social](https://gamemaking.social) - amateur videogame making: game writers, game players welcome
* [gameliberty.club](https://gameliberty.club) - mainly gaming / nerd instance
* [pcgamer.social](https://pcgamer.social) - by PC gamer, for PC gamers

#### 🔴 [Political and social views](#political-and-social-views)
* [todon.nl](https://todon.nl) - for leftists, socialists, anarchists, Marxists, social democrats, social liberals
* [hispagatos.space](https://hispagatos.space)- for hackers, social anarchists, and anarchist hackers
* [anarchism.space](https://anarchism.space) - for social anarchists
* [sunbeam.city](https://sunbeam.city) - libertarian Socialist and solarpunk community
* [activism.openworlds.info](https://activism.openworlds.info) - for activists
* [campaign.openworlds.info](https://campaign.openworlds.info) - for campaigners and NGOs

#### 🐰 [Ecology and animals](#ecology-and-animals)
* [donteatanimals.org](https://donteatanimals.org) - discussion of animal rights and veganism
* [toot.cat](https://toot.cat) - instance for cats, the people who love them, and kindness in general
* [waterfowl.social](https://waterfowl.social) - for waterfowl lovers who keep ducks, geese, and other waterfowl as pets

#### 🐒 [Family / Social relations](#social)
* [dads.cool](https://dads.cool) - anyone with a kid can be a dad
* [cooler.mom](https://cooler.mom) - anyone with a kid can be a mom

#### 🌈 [Safe spaces](#safe-spaces)
* [tech.lgbt](https://tech.lgbt) - for tech workers, academics, students, and others in tech who are LGBTQA+ allies
* [eldritch.cafe](https://eldritch.cafe) - for queer, feminist and anarchist people
* [deadinsi.de](https://deadinsi.de) - anti-fascist, LGBT friendly
* [colorid.es](https://colorid.es)- LGBTQIA+ / queer instance primarily for Portuguese speakers
* [pipou.academy](https://pipou.academy) - emphasis on kindness
* [notbird.site](https://notbird.site)

#### 👽 [Fandoms](#fandoms)
* [fandom.ink](https://fandom.ink) - for fans and fandoms of all types

#### 🐾 [Subcultures](#subcultures)
* [this.mouse.rocks](https://this.mouse.rocks) - furry instance for all the mouse adjacent folks who rock
* [vulpine.club](https://vulpine.club) - community for foxes, friends, and other furry (and non-furry) creatures

#### 🌏 [Notable generalistic](#notable-generalistic)
// *small-to-medium sized instances that will be happy to have new users*
* [kafuka.me](https://kafuka.me)
* [kernel.social](https://kernel.social)
* [libertalia.world](https://libertalia.world) - named after a legendary colony forged by pirates
* [nerdculture.de](https://nerdculture.de)
* [weirder.earth](https://weirder.earth) - for thoughtful weirdos
* [merveilles.town](https://merveilles.town)
* [projectx.fun](https://projectx.fun) - will be open for up to 100 users, for sanity and sustainability
* [nulled.red](https://nulled.red) - refuses to block or silence any instances
* [ieji.de](https://ieji.de) - no logs, has Tor .onion address
* [mstdn.fr](https://mstdn.fr) - by sysadmins, with a goal to provide robust experience for tens of thousands of users
* [social.tchncs.de](https://social.tchncs.de) - one of the oldest instances

#### 😸 [For people with a sense of self-irony](#for-people-with-a-sense-of-self-irony)
* [ephemeral.glitch.social](https://ephemeral.glitch.social) - toots are ephemeral and disappear after a while
* [botsin.space](https://botsin.space) - for bots

#### 🐚 [Run by tech-savvy organizations](#run-by-tech-savvy-organizations)
* [en.osm.town](https://en.osm.town) - for the OpenStreetMap Community
* [mastodon.hackerlab.fr](https://mastodon.hackerlab.fr) - by Hackerlab
* [mi.pede.rs](https://mi.pede.rs) - by Hacklab-in-Mama hackerspace, Zagreb
* [social.weho.st](https://social.weho.st) - by WeHost, non-profit internet service provider, Amsterdam
* [m.g3l.org](https://m.g3l.org) - by G3L, libre software association
* [mastodon.nzoss.nz](https://mastodon.nzoss.nz) - by New Zealand Open Source Society
* [mamot.fr](https://mamot.fr) - by Quadrature du Net, association for the defence of digital rights and freedoms
* [mastodon.roflcopter.fr](https://mastodon.roflcopter.fr) - by Roflcopter, [CHATONS](https://chatons.org) member
* [mastodon.zaclys.com](https://mastodon.zaclys.com) - by Zaclys, association francophone
* [libretooth.gr](https://libretooth.gr) - by LibreOps, who contribute to (re-)decentralizing the net

#### 🎉 [Notable mention](#notable-mention)
// *some are open only to particular audience, i.e., to students of university*
* [mastodon.mit.edu](https://mastodon.mit.edu) - for the MIT community
* [mastodon.ocf.berkeley.edu](https://mastodon.ocf.berkeley.edu) - for Berkeley students, faculty, and staff
* [mastodon.acc.sunet.se](https://mastodon.acc.sunet.se) - by Academic Computer Club at Umeå University, Sweden
* [mastodon.utwente.nl](https://mastodon.utwente.nl) - for the University of Twente community
* [mastodon.iut-larochelle.fr](https://mastodon.iut-larochelle.fr) - by University of La Rochelle (Closed Reg.)
* [w3c.social](https://w3c.social) - for people involved in the activities of the World Wide Web Consortium (Closed Reg.)
* [mastodon.greenpeace.ch](https://mastodon.greenpeace.ch) - by Greenpeace Switzerland (Closed Reg.)

## Disclaimer
`Following instances skipped`: closed registration *at the moment of checking*, running old code, theme / description not understood due to language barrier or scarce description, long blocklists, experimental servers.

Add your own themed instance via [merge request](https://gitlab.com/distributopia/masto-world-overview). If your server already has many users, it will be nice of you to let other servers grow, for a healthier decentralization.
</ul>

## 🌟 Other research links
- [Friendica world overview](https://gitlab.com/distributopia/friendica-world-overview)
- [Fediverse pirate servers](https://gitlab.com/distributopia/caramba)
- [Mastodon server distribution](https://chaos.social/@leah/99837391793032137) research by Leah
- [Fediverse server distribution](https://fediverse.network/servers) by href
