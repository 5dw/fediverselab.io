
---
layout: "post"
title: "Call for Participation"
date: 2019-07-20
tags:
    - hubzilla
preview: "Join the efforts to standardize Zot protocol, used in Hubzilla platform. This is a community initiative to push Zot adoption for federated social web."
url: "https://www.w3.org/community/zot"
lang: en
wanted: true
---

Join the efforts to standardize the Zot protocol, currently used in Hubzilla and Zap platforms. This is a community initiative to push Zot adoption for federated social web.

Initial call for participation was published [here](https://www.w3.org/community/zot) ([RSS](https://www.w3.org/community/zot/feed)).
