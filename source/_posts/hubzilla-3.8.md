
---
layout: "post"
title: "Hubzilla 3.8 release"
date: 2018-10-19
tags:
    - hubzilla
preview: "Hubzilla 3.8 released this morning. The entire apps and settings infrastructure was re-organised, simplifying the settings and improving general UX"
url: "https://hub.somaton.com/channel/mario/?f=&mid=819074605423c8e62856528cb91d2d2a3e5364f9014ce01d4ffa2faafb68b7b5@hub.somaton.com"
lang: en
---

Hubzilla 3.8 released this morning. The entire apps and settings infrastructure was re-organised, simplifying the settings and improving general UX.

Among the notable changes you'll find: markdown table support, new addon which provides a reputation system for community channels, new addon that implements a WYSIWYG editor, new subscriptions submodule for Cart addon which provides online store functionality.

Read the official [announcement](https://hub.somaton.com/channel/mario/?f=&mid=819074605423c8e62856528cb91d2d2a3e5364f9014ce01d4ffa2faafb68b7b5@hub.somaton.com).
