
---
layout: "post"
title: "Friendica hackathon 2018"
date: 2018-11-13
tags:
    - friendica
preview: "Join Friendica hackathon this weekend, offline in Berlin, or online! Hacking will be focused on the open issues of the upcoming 2018.12 release"
url: "https://friendi.ca/2018/11/13/friendica-hackathon-2018"
lang: en
---

Join Friendica hackathon this weekend, offline in Berlin, or online.
Work will be focused on the open issues of the upcoming 2018.12 release.
Read the [announcement](https://friendi.ca/2018/11/13/friendica-hackathon-2018) for contacts and details.

Happy hacking!
