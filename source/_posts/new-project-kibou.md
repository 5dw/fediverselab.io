
---
layout: "post"
title: "Kibou joins Fediverse"
date: 2019-04-02
tags:
    - kibou
    - fediverse
preview: "New project Kibou joins Fediverse. Still in early development stage, it aims to be a highly customizable multi-protocol social networking server"
url: "https://git.cybre.club/kibouproject/kibou"
lang: en
---

New project [Kibou](https://git.cybre.club/kibouproject/kibou) joins Fediverse. Written in Rust and still in early development stage, it aims to be a highly customizable social networking server. Even better, it plans to be multi-protocol. There are a couple of [servers](https://the-federation.info/kibou) online, federation is known to work with Pleroma, Misskey and Mastodon.

Another new project may be looming on the horizon, a "Postessence" testing server was spotted at [The-Fedetation.info](https://the-federation.info). The project is evidently in "suspense" [stage](http://hivemind.gq).
